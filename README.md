# BadSinesGame

## About

Bad Sines is a simple browser game that involves guessing the amplitude of 8 sine functions of different frequencies such that their combination results in the provided graph of a seemingly more complex function. Ideally, this game will lend insight into how functions can be decomposed into their frequency components, which is the underlying idea behind the ubiquitous Fourier and Fourier-like transforms.

## Usage
The badSines.html file contains the whole game, so you can simply download and open this file in your browser to play it. Alternatively, it is also free to play at: https://sacrificialprawn.itch.io/bad-sines
<br> Note that there is an in-game tutorial that goes through how to play the game in more detail. 

## License
This project is release under the MIT license.
